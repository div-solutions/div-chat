<div align="center">
  <img width="600" src="https://i.imgur.com/a047clp.png">
  <hr>
</div>

# Description
divchat is a real-time open source chat. Can be integrated efortless with website or app. It's an alternative to any Real-time chat such as Facebook, Whatsapp, Signal, etc. You can keep control over your conversations in personal or professional enviroment. Forget about checking any third party apps, integrate your app with divchat!

Created by div.pt.

Used by: 
- [Simplan](https://simplan.io)`

# Features
*  Direct personal messages;
*  Overlay template;
*  Timer in messages;
*  Messages history;
*  Avatar display for each user;
*  Organize alphabetically or by online first;
*  Online/offline icon;

# Roadmap
*  Dark theme;
*  Full page layout;
*  Group messages;
*  Emojis;
*  Push notificatons;
*  Sound when receive message;
*  Group chat;

# Using

1. `npm install div-chat`
2. Add to app.module.ts
```
import { DivChatModule } from 'div-chat'

...
imports: [
... 
DivChatModule
]
```

3. Add it to app.component.html
```
<div-chat 
    (chatOpenHandler)="chatOpenHandler($event)"
    (sentMessageHandler)="sentMessageHandler($event)" 
    (groupCreatedHandler)="groupCreated($event)" 
    (groupUpdatedHandler)="groupUpdated($event)" 
    [userId]="userId" 
></div-chat>
```

4. Add to app.component.ts
```
chatOpenHandler(data) {
    this.hubConnection.invoke(`FetchHistory`, this.token, data.id, data.entityType);
  }

  groupCreated(value) {
    this.hubConnection.invoke(`CreateGroup`, value);
  }

  groupUpdated(value) {
    this.hubConnection.invoke(`UpdateGroup`, value);
  }

  sentMessageHandler(value) {
    this.hubConnection.invoke(`IncomingMessage`, this.token, value).then(() => {
      console.log('msg sent!', value);
    });
  }
  
  // For SignalR
hubConnection: signalR.HubConnection

initSignalR() {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/chatHub`)
      .build();

    this.hubConnection
      .start()
      .then(() => {
        this.hubConnection.invoke('ClientConnected', this.token);
        this.initializeListeners();
      })
      .catch(err => console.log(`Error while starting SignalR connection: ${err}`));
  }

  private initializeListeners(): void {
    this.hubConnection.on('UpdateUsers', (list) => {
      var newList = list.map(x => new UserModel({
        id: x.id,
        name: x.name,
        status: x.status,
        avatar: `{x.avatar}`,
        unreadMessagesNumber: x.unreadMessagesCount,
      }))
      this.activeUsersService.updateList(newList);
    });

    this.hubConnection.on('UpdateGroupsResult', (list) => {
      var newList = list.map(x => {
        var group = new GroupModel({ id: x.id, name: x.name });
        group.userIds = x.userIds;
        return group;
      });
      this.activeGroupsService.updateList(newList);
    });

    this.hubConnection.on('IncomingMessageResult', (entityType, from, message) => {
      console.log('result came', entityType, from, message)
      var entity = entityType == 1 ? this.activeUsersService.getItem(from) : this.activeGroupsService.getItem(from);
      this.activeWindowsService.openWindow(entity);
      this.activeWindowsService.list.filter(x => x.entity.id == from)[0].messages.push(message);
      this.activeWindowsService.list.filter(x => x.entity.id == from)[0].isLoading.emit(false);
    });

    this.hubConnection.on('FetchMessagesResult', (to, messages) => {
      console.log('result back', to, messages)
      if (messages != undefined) this.activeWindowsService.list.filter(x => x.entity.id == to)[0].messages = messages;
      this.activeWindowsService.list.filter(x => x.entity.id == to)[0].isLoading.emit(false);
    });
  }
```





