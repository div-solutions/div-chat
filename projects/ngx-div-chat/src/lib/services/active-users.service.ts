import { Injectable, EventEmitter } from '@angular/core';
import { UserModel } from '../models/user-model';
import { Observable, of } from 'rxjs';
import { ActiveWindowsService } from './active-windows.service';
import { EntityType } from '../models/entity-base-model';

@Injectable({
  providedIn: 'root'
})
export class ActiveUsersService {

  readonly list: EventEmitter<UserModel[]> = new EventEmitter();
  private _list: UserModel[] = [];

  constructor(
    private activeWindowsService: ActiveWindowsService,
  ) { }

  getItem(id) {
    return this._list.filter(x => x.id == id)[0];
  }

  getList() {
    return this._list;
  }

  updateList(list: UserModel[]) {
    list.forEach(x => {
      x.entityType = EntityType.User
    });
    
    this.list.emit(list);
    this._list = list;

    // Update active windows user status
    list.forEach(item => {
      var activeWindowItem = this.activeWindowsService.list.filter(x => x.entity.id == item.id);
      if (activeWindowItem.length > 0) {
        this.activeWindowsService.list.filter(x => x.entity.id == item.id)[0].entity = item;
        this.activeWindowsService.list.filter(x => x.entity.id == item.id)[0].isLoading.emit(false);
      }
    })
  }


}
