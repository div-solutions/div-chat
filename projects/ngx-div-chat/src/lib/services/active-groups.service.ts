import { Injectable, EventEmitter } from '@angular/core';
import { GroupModel } from '../models/group-model';
import { ActiveWindowsService } from './active-windows.service';
import { EntityType } from '../models/entity-base-model';
import { ActiveUsersService } from './active-users.service';

@Injectable({
  providedIn: 'root'
})
export class ActiveGroupsService {

  readonly list: EventEmitter<GroupModel[]> = new EventEmitter();
  private _list: GroupModel[] = [];

  constructor(
    private activeWindowsService: ActiveWindowsService,
    private activeUsersService: ActiveUsersService
  ) { }

  getItem(id) {
    return this._list.filter(x => x.id == id)[0];
  }

  getList() {
    return this._list;
  }

  updateList(list: GroupModel[]) {
    list.forEach(x => {
      x.userIds.forEach(userId => {
        x.users.push(this.activeUsersService.getItem(userId))
      });
      x.entityType = EntityType.Group
    });

    this._list = list;
    this.list.emit(list);

    // Update active windows user status
    list.forEach(item => {
      var activeWindowItem = this.activeWindowsService.list.filter(x => x.entity.id == item.id);
      if (activeWindowItem.length > 0) {
        this.activeWindowsService.list.filter(x => x.entity.id == item.id)[0].entity = item;
        this.activeWindowsService.list.filter(x => x.entity.id == item.id)[0].isLoading.emit(false);
      }
    })
  }

  insertGroup(item: GroupModel) {
    this._list.push(item);
    this.updateList(this._list);
  }

  updateGroup(item: GroupModel) {
    this._list.filter(x => x.id == item.id)[0].name = item.name;
    this._list.filter(x => x.id == item.id)[0].users = item.users;
    this._list.filter(x => x.id == item.id)[0].userIds = item.userIds;
    this.updateList(this._list);
  }
}
