import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActiveMessagesService {

  listModified: EventEmitter<boolean> = new EventEmitter(false);
  list = [];

  constructor() { }
}
