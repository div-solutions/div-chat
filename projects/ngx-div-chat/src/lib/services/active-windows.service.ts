import { Injectable, Output, EventEmitter } from '@angular/core';
import { WindowModel } from '../models/window-model';
import { EntityBaseModel } from '../models/entity-base-model';

@Injectable({
  providedIn: 'root'
})
export class ActiveWindowsService {

  listModified: EventEmitter<boolean> = new EventEmitter(false);
  list: WindowModel[] = [];

  constructor() { }

  openWindow(user) {
    if (this.list.filter(x => x.entity.id == user.id).length > 0) return;

    if (this.list.length >= 4)
      this.list.shift();

    this.list.push(new WindowModel(new EntityBaseModel(user)));
    this.listModified.emit(true);
  }

  newMessage(entityId, message, playSound = false) {
    this.list.filter(x => x.entity.id == entityId)[0].messages.push(message);
    this.list.filter(x => x.entity.id == entityId)[0].isLoading.emit(false);

    if(playSound == true) {
      let audio = new Audio();
      audio.src = "/assets/insight.mp3";
      audio.load();
      audio.play();
    }
  }


  
}
