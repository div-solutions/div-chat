import { EventEmitter } from '@angular/core';
import { UserModel } from './user-model';
import { EntityBaseModel } from './entity-base-model';

export class WindowModel {

    public isLoading: EventEmitter<boolean> = new EventEmitter(true);

    public entity: EntityBaseModel;

    public messages = [];

    public index: Number;

    constructor(entity: EntityBaseModel) {
        this.entity = entity;
    }

}
