import { EntityBaseModel, IEntityBaseModel } from './entity-base-model';
import { UserModel } from './user-model';

export class GroupModel extends EntityBaseModel {

    users: UserModel[] = [];

    userIds: any[] = [];

    constructor(obj: IEntityBaseModel, userIds = []) {
        super(obj);
        this.userIds = userIds;
    }
}
