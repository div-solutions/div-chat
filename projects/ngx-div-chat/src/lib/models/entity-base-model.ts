import { EventEmitter } from '@angular/core';

export interface IEntityBaseModel {
    id: string;

    name: string;

    avatar?: string;

    status?: Status;

    unreadMessagesNumber?: Number;

    entityType?: EntityType;
}

export class EntityBaseModel {
    public id: string;

    public name: string;

    public avatar: string;

    public status: Status;

    public unreadMessagesCount: Number;

    public entityType: EntityType;

    constructor(obj: IEntityBaseModel){
        this.entityType = obj.entityType || EntityType.User;
        this.id = obj.id;
        this.name = obj.name;
        this.status = (obj.status == undefined ? 2 : obj.status);
        this.avatar = obj.avatar || '';
        this.unreadMessagesCount = obj.unreadMessagesNumber || 0;
    }
}

export enum Status {
    Online = 1,
    Offline = 2,
}

export enum EntityType {
    User = 1,
    Group = 2,
    PublicGuid = 3
}