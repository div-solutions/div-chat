import { EntityBaseModel, EntityType, IEntityBaseModel } from './entity-base-model';

export class UserModel extends EntityBaseModel {

    constructor(obj: IEntityBaseModel) {
        super(obj);
    }

}
