import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { NgxDivChatComponent } from './ngx-div-chat.component';
import { MainComponent } from './views/main/main.component';
import { ChatBoxComponent } from './views/chat-box/chat-box.component';
import { MomentModule } from 'ngx-moment';
import { AddEmojiComponent } from './views/components/add-emoji/add-emoji.component';
import { GroupSettingsComponent } from './views/components/group-settings/group-settings.component';
import { StatusBadgeComponent } from './views/components/status-badge/status-badge.component';
import { DeviceDetectorModule } from 'ngx-device-detector';

@NgModule({
  declarations: [
    NgxDivChatComponent, 
    MainComponent, 
    ChatBoxComponent, 
    AddEmojiComponent, 
    GroupSettingsComponent, 
    StatusBadgeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgZorroAntdModule,
    MomentModule, 
    DeviceDetectorModule, 
  ],
  exports: [
    NgxDivChatComponent
  ], 
  entryComponents: [
    GroupSettingsComponent
  ]
})
export class NgxDivChatModule { }
