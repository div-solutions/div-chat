import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ActiveWindowsService } from './services/active-windows.service';
import { ActiveUsersService } from './services/active-users.service';
import { ActiveGroupsService } from './services/active-groups.service';
import { UserModel } from './models/user-model';
import { GroupModel } from './models/group-model';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'div-chat',
  templateUrl: './ngx-div-chat.component.html',
  styles: ['./ngx-div-chat.component.css']
})
export class NgxDivChatComponent implements OnInit {
  activeWindows = [];

  @Input() userId = "1";
  @Input() uiSettings;
  @Input() isCollapsed = true;

  @Input() showCreator = true;
  @Input() showTestData = false;

  @Output() chatOpenHandler = new EventEmitter();
  @Output() sentMessageHandler = new EventEmitter();
  @Output() groupCreatedHandler = new EventEmitter();
  @Output() groupUpdatedHandler = new EventEmitter();

  constructor(
    private activeWindowsService: ActiveWindowsService,
    private activeUsersService: ActiveUsersService,
    private deviceService: DeviceDetectorService,
    private activeGroupsService: ActiveGroupsService,
  ) { }

  ngOnInit(): void {
    this.getDeviceType();
    this.activeWindowsService.listModified.subscribe(() => {
      this.activeWindows = this.activeWindowsService.list.map(x => ({ ...x, index: this.activeWindowsService.list.indexOf(x) + 1 }));
    });

    if(this.isCollapsed) localStorage.setItem(`divchat_mainWindowCollapsed`, this.isCollapsed.toString())

    if (this.userId == null || this.userId == undefined) {
      console.error('Please provide user id. [userId]="XXX" ')
    } else
      localStorage.setItem(`divchat_userId`, this.userId);

    if (this.showTestData == true) {
      setTimeout(() => {
        this.activeUsersService.updateList([
          new UserModel({ id: '1', name: 'Admin with super long name', status: 1, avatar: 'https://upload.wikimedia.org/wikipedia/commons/a/a3/June_odd-eyed-cat.jpg', unreadMessagesNumber: 3 }),
          new UserModel({ id: '2', name: 'Vasco', status: 1, avatar: 'https://upload.wikimedia.org/wikipedia/commons/d/dc/Young_cats.jpg', unreadMessagesNumber: 2 }),
          new UserModel({ id: '3', name: 'test', status: 1, avatar: 'https://upload.wikimedia.org/wikipedia/commons/d/dc/Young_cats.jpg', unreadMessagesNumber: 999 }),
        ]);
        this.activeGroupsService.updateList([
          new GroupModel({ id: 'g1', name: 'group 1' }, ['1', '2', '3'])
        ]);
      }, 1000);
    }
  }

  chatOpenInnerHandler(id) {
    if (this.showTestData == true) {
      this.activeWindowsService.list.filter(x => x.entity.id == 'g1')[0].messages.push({ fromId: '1', toId: 'g1', type: '2', message: 'msg from admin with long', dateSend: new Date() });
      this.activeWindowsService.list.filter(x => x.entity.id == 'g1')[0].messages.push({ fromId: '2', toId: 'g1', type: '2', message: 'msg from Vasco', dateSend: new Date() });
      this.activeWindowsService.list.filter(x => x.entity.id == 'g1')[0].messages.push({ fromId: '2', toId: 'g1', type: '2', message: 'msg from Vasco', dateSend: new Date() });
      this.activeWindowsService.list.filter(x => x.entity.id == 'g1')[0].messages.push({ fromId: '2', toId: 'g1', type: '2', message: 'msg from Vasco some long text asd dasfhuihfds iughiudfgn odgfhidugbdf ndngfg oidjngdfg oidjogfd', dateSend: new Date() });
      this.activeWindowsService.list.filter(x => x.entity.id == 'g1')[0].isLoading.emit(false);
    }
    this.chatOpenHandler.emit(id);
  }

  sentMessageInnerHandler(value) {
    this.sentMessageHandler.emit(value);
  }

  groupCreatedInnerHandler(value) {
    this.groupCreatedHandler.emit(value);
  }

  groupUpdatedInnerHandler(value) {
    this.groupUpdatedHandler.emit(value);
  }

  private getDeviceType() {
    if (this.deviceService.isMobile()) localStorage.setItem(`divchat_deviceType`, `mobile`);
    if (this.deviceService.isTablet()) localStorage.setItem(`divchat_deviceType`, `tablet`);
    if (this.deviceService.isDesktop()) localStorage.setItem(`divchat_deviceType`, `desktop`);
  }

}
