import { Component, OnInit, ElementRef, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import { ActiveUsersService } from '../../services/active-users.service';
import { ActiveWindowsService } from '../../services/active-windows.service';
import { GroupSettingsComponent } from '../components/group-settings/group-settings.component';

@Component({
  selector: 'DivChat-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.css']
})
export class ChatBoxComponent implements OnInit {
  @Input() screenPositionNo = 1;
  @Input() entity;

  @Output() windowOpenHandler = new EventEmitter();
  @Output() sentMessageHandler = new EventEmitter();
  @Output() groupUpdatedHandler = new EventEmitter();

  userId = '1';

  isLoading = true;
  messages = [];
  message = '';

  @ViewChild('scrollMe', { static: false }) private myScrollContainer: ElementRef;

  constructor(
    private activeWindowsService: ActiveWindowsService,
    private activeUsersService: ActiveUsersService,
    private modalService: NzModalService,
  ) { }

  ngOnInit() {
    this.userId = localStorage.getItem(`divchat_userId`);
    var item = this.activeWindowsService.list.filter(x => x.entity.id == this.entity.id)[0];

    item.isLoading.subscribe(r => {
      this.isLoading = r;
      this.messages = item.messages;

      if (this.entity.entityType === 2 || this.entity.chatOpenHandler === 3) { // It group message
        this.messages.forEach(message => {
          message.from = this.activeUsersService.getItem(message.fromId);
        });
      }

      this.entity = this.activeWindowsService.list.filter(x => x.entity.id == this.entity.id)[0].entity;

      setTimeout(() => { // Scroll to bottom, needs delay for data to init.
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      }, 200)
    })
    this.windowOpenHandler.emit({ id: item.entity.id, entityType: item.entity.entityType });
  }

  sendMessage() {
    if(this.message == '') return;
 
    let messageItem = { fromId: this.userId, toId: this.entity.id, message: this.message, entityType: this.entity.entityType, dateSent: new Date() };
    this.activeWindowsService.newMessage(this.entity.id, messageItem, false);
    this.sentMessageHandler.emit(messageItem)

    this.message = '';
  }

  showGroupSettingsModal() {
    var modal = this.modalService.create({
      nzTitle: 'Group settings',
      nzContent: GroupSettingsComponent,
      nzFooter: null,
      nzComponentParams: {
        groupId: this.entity.id
      }
    });
    modal.afterClose.subscribe(r => {
      if (r == undefined) return; // Closed with button or X
      this.groupUpdatedHandler.emit(r);
    })
  }

  submit(event) {
    if (event.keyCode == 13)
      this.sendMessage();
  }

  iconClicked(icon) {
    this.message += icon;
  }


  close() {
    this.activeWindowsService.list = this.activeWindowsService.list.filter(x => x.entity.id != this.entity.id);
    this.activeWindowsService.listModified.emit(true);
  }

}
