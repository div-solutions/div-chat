import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { NzModalService, toBoolean } from 'ng-zorro-antd';
import { UserModel } from '../../models/user-model';

import { ActiveWindowsService } from './../../services/active-windows.service'
import { ActiveUsersService } from './../../services/active-users.service'
import { ActiveGroupsService } from './../../services/active-groups.service'
import { GroupSettingsComponent } from '../components/group-settings/group-settings.component';

@Component({
  selector: 'DivChat-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {
  @Output() groupCreatedHandler = new EventEmitter();
  @Input() showCreator = true;

  isLoading = false;
  isCollapsed = false;

  chatSettingsVisible = false;
  groupSettingsVisible = false;

  sortBy = 'alphabet';
  currentTab = 'users';
  searchValue = '';

  title = "Collaborators";

  groups: UserModel[] = [];
  users: UserModel[] = [];
  filteredItems: any[] = [];

  groupsEnabled = true;

  constructor(
    private activeWindowsService: ActiveWindowsService,
    private activeUsersService: ActiveUsersService,
    private activeGroupsService: ActiveGroupsService,
    private cdr: ChangeDetectorRef,
    private modalService: NzModalService
  ) { }

  ngOnInit() {
    this.isCollapsed = toBoolean(localStorage.getItem(`divchat_mainWindowCollapsed`) || false)
    this.sortBy = (localStorage.getItem('divchat_sortBy') || 'alphabet');

    this.isLoading = false;
    this.activeUsersService.list.subscribe(r => {
      this.users = r.filter(x => x.id != localStorage.getItem(`divchat_userId`));
      if (this.currentTab == 'users') {
        this.applySettings();
      }
    });

    this.activeGroupsService.list.subscribe(r => {
      this.groups = r;
      if (this.currentTab == 'groups') {
        this.applySettings();
      }
    });
  }

  openChat(user) {
    this.activeWindowsService.openWindow(user);
  }

  search() {
    var source = this.currentTab == 'users' ? this.users : this.groups;

    if (this.searchValue == '') this.filteredItems = source;
    else this.filteredItems = source.filter(x => x.name.toLowerCase().includes(this.searchValue.toLowerCase()));
  }

  tabChanged(name) {
    setTimeout(() => {
      this.currentTab = name;
      this.searchValue = '';
      this.applySettings();
    }, 0);
  }

  showCreateGroupModal() {
    var modal = this.modalService.create({
      nzTitle: 'Create group',
      nzContent: GroupSettingsComponent,
      nzFooter: null
    });
    modal.afterClose.subscribe(r => {
      if (r == undefined) return; // Closed with button or X
      this.groupCreatedHandler.emit(r);
    })
  }

  applySettings() {
    this.isLoading = true;
    var source = this.currentTab == 'users' ? this.users : this.groups;

    switch (this.sortBy) {
      case 'alphabet':
        this.filteredItems = source.sort(function (a, b) {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        })
        break;
      case 'status':
        this.filteredItems = source.sort(function (a, b) {
          if (a.status < b.status) { return -1; }
          if (a.status > b.status) { return 1; }
          return 0;
        })
        break;
    }

    localStorage.setItem('divchat_sortBy', this.sortBy);
    this.chatSettingsVisible = false;
    this.cdr.detectChanges();
    this.search();
    this.isLoading = false;
  }

  showHideWindow() {
    this.isCollapsed = !this.isCollapsed;
    localStorage.setItem(`divchat_mainWindowCollapsed`, this.isCollapsed.toString())
  }

}
