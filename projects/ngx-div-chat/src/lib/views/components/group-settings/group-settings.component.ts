import { Component, OnInit, Input } from '@angular/core';
import { ActiveUsersService } from './../../../services/active-users.service';
import { ActiveGroupsService } from './../../../services/active-groups.service';
import { GroupModel } from './../../../models/group-model';
import * as uuid from 'uuid';
import { NzModalRef } from 'ng-zorro-antd';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'DivChat-group-settings',
  templateUrl: './group-settings.component.html',
  styleUrls: ['./group-settings.component.css']
})
export class GroupSettingsComponent implements OnInit {
  @Input() groupId;

  isLoading = true;

  users = [];

  form: FormGroup;

  constructor(
    private activeUsersService: ActiveUsersService,
    private activeGroupService: ActiveGroupsService,
    private activeModal: NzModalRef,
  ) {
    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
      selectedUsers: new FormControl([], Validators.required),
    });
  }

  ngOnInit() {
    if (this.groupId != undefined) {
      var group = this.activeGroupService.getItem(this.groupId);

      this.form.controls["title"].setValue(group.name);
      this.form.controls["selectedUsers"].setValue(group.userIds);
    }

    this.users = this.activeUsersService.getList().filter(x => x.id != localStorage.getItem(`divchat_userId`));
    this.isLoading = false;
  }

  createGroup() {
    if (!this.form.valid) return;

    this.isLoading = true;
    var group = new GroupModel({ id: (this.groupId || uuid.v4()), name: this.form.controls['title'].value, status: 0, avatar: 'def' });
    group.userIds = this.form.controls['selectedUsers'].value;

    if (this.groupId == undefined) {
      group.userIds.push(localStorage.getItem(`divchat_userId`))
      group.userIds = group.userIds.reverse();
      this.activeGroupService.insertGroup(group);
    } else {
      this.activeGroupService.updateGroup(group);
    }
    this.activeModal.close(group)
  }

  close() {
    this.activeModal.destroy();
  }
}
