import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'DivChat-status-badge',
  templateUrl: './status-badge.component.html',
  styleUrls: ['./status-badge.component.css'], 
  encapsulation: ViewEncapsulation.None
})
export class StatusBadgeComponent implements OnInit {

  @Input() status;

  constructor() { }

  ngOnInit() {
  }

}
