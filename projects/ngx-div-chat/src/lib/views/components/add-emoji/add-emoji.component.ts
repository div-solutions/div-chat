import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash'


@Component({
  selector: 'DivChat-add-emoji',
  templateUrl: './add-emoji.component.html',
  styleUrls: ['./add-emoji.component.css']
})
export class AddEmojiComponent implements OnInit {
  @Output() iconClickedHandler = new EventEmitter();

  tabs = [];

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.http.get(`./../../../assets/emoji.json`).subscribe(icons => {
      var iconsGrouped = _.groupBy(icons, x => x['group']);
      _.forEach(iconsGrouped, iconGrouped => {
        this.tabs.push({ icon: iconGrouped[0]["char"], icons: iconGrouped, });
      })
      this.tabs = _.filter(this.tabs, x => x.icon != "👋" && x.icon != "🏻" && x.icon != "🏁");
    })
  }

  click(icon) {
    this.iconClickedHandler.emit(icon);
  }
}
