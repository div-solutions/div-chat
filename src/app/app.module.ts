import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DivChat } from './app.component';

import { MomentModule } from 'ngx-moment';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { DeviceDetectorModule, DeviceDetectorService } from 'ngx-device-detector';
import { CustomAvatarComponent } from './lib/views/components/custom-avatar/custom-avatar.component';
import { CustomStatusBadgeComponent } from './lib/views/components/custom-status-badge/custom-status-badge.component';
import { MainComponent } from './lib/views/main/main.component';
import { FullscreenComponent } from './lib/views/fullscreen/fullscreen.component';
import { EmbeddedComponent } from './lib/views/embedded/embedded.component';
import { ChatBoxComponent } from './lib/views/chat-box/chat-box.component';
import { ChatBoxContentComponent } from './lib/views/chat-box-content/chat-box-content.component';
import { ParticipantsTabsComponent } from './lib/views/participants-tabs/participants-tabs.component';

@NgModule({
  declarations: [
    DivChat,
    CustomAvatarComponent,
    CustomStatusBadgeComponent,
    MainComponent,
    FullscreenComponent,
    EmbeddedComponent,
    ChatBoxComponent,
    ChatBoxContentComponent,
    ParticipantsTabsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MomentModule,
    NgZorroAntdModule,
    DeviceDetectorModule,
  ],
  providers: [
    DeviceDetectorService
  ],
  bootstrap: [DivChat],
  exports: [DivChat],
  entryComponents: [
    ]
})
export class DivChatModule { }
