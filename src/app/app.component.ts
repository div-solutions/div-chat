import { Component, OnInit, Input } from '@angular/core';
import { Status } from './lib/models/status-enum';

import { MainService } from './lib/services/main.service';

enum TabType {
  Users = 1,
  PrivateGroups = 2,
  OpenGroups = 3,
}

@Component({
  selector: 'div-chat-old',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class DivChat implements OnInit {

  @Input() settings;
  fullscreenMode;
  embeddedModel;

  constructor(
    private mainService: MainService
  ) { }


  ngOnInit(): void {
    this.settings = {
      windows: {
        main: {
          width: 350,
          height: 500,
          isVisible: true
        },
        fullscreen: {
          margintop: 0
        }
      },
      // activeChats: [
      //   { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Online },
      //   { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Online },
      //   { id: 'xx', name: 'Vasco L 4', avatar: 'https://scontent-waw1-1.xx.fbcdn.net/v/t31.0-1/cp0/c0.7.60.60a/p60x60/21640903_1136793516420779_6930045231686576651_o.jpg?_nc_cat=102&_nc_sid=7206a8&_nc_ohc=8YZRzCRczMYAX_j4RGu&_nc_ht=scontent-waw1-1.xx&oh=9da276219e259b81874345b53e61a309&oe=5EEBF675', status: Status.Online },
      //   { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Online },
      // ]
    };
    this.settings.windows.main.currentHeight = this.settings.windows.main.height;
    this.mainService.settings = this.settings;
    this.fullscreenMode = true;
    this.embeddedModel = false;
    this.mainService.userId = '1';

    this.mainService.tabs = [
      {
        id: 'friends', name: "Friends", icon: "fas fa-user-friends", type: 1, list: [
          { id: 'xx', name: 'Vasco L 1', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 2', avatar: '', status: Status.Online },
          { id: 'xx', name: 'Vasco L 3', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Away, minutesAway: 10 },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Online },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },
          { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Offline },

        ]
      },
      // {
      //   id: 'private_groups', name: "Private groups", icon: "fab fa-accessible-icon", type: 2, list: [
      //     { id: 'xx', name: 'Vasco Group 1', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613' },
      //     { id: 'xx', name: 'Vasco Group 2', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613' },
      //     { id: 'xx', name: 'Vasco Group 3', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613' },
      //     { id: 'xx', name: 'Vasco Group 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613' },
      //   ]
      // },
      // {
      //   id: 'public_chats', name: "Public chats", icon: "fab fa-accessible-icon", type: 3, list: [
      //     { id: 'xx', name: 'Global', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613' },
      //     { id: 'xx', name: 'Markets', avatar: '' },
      //     { id: 'xx', name: 'Country', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613' },
      //   ]
      // },
    ];

    // this.settings.windows.main.isVisible = !this.settings.windows.main.isVisible;
    // this.toggleCollapse();
    // this.selectedTab = this.tabs[0];
  }











  // selectTab(id) {
  //   this.selectedTab = this.tabs.filter(x => x.id === id)[0];
  // }

  // ngOnInit(): void {
  //   this.selectedTab = this.tabs[0];
  //   this.settings = {
  //     windows: {
  //       main: {
  //         width: 350,
  //         height: 700,
  //         isVisible: true
  //       },
  //       fullscreen: {
  //         margintop: 17
  //       }
  //     },
  //     activeChats: [
  //       { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Online },
  //       { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Online },
  //       { id: 'xx', name: 'Vasco L 4', avatar: 'https://scontent-waw1-1.xx.fbcdn.net/v/t31.0-1/cp0/c0.7.60.60a/p60x60/21640903_1136793516420779_6930045231686576651_o.jpg?_nc_cat=102&_nc_sid=7206a8&_nc_ohc=8YZRzCRczMYAX_j4RGu&_nc_ht=scontent-waw1-1.xx&oh=9da276219e259b81874345b53e61a309&oe=5EEBF675', status: Status.Online },
  //       { id: 'xx', name: 'Vasco L 4', avatar: 'https://drive.google.com/uc?id=1klPumVDwMaBeiKTJyRZwmIqSShJQU613', status: Status.Online },
  //     ]
  //   }
  //   this.settings.windows.main.isVisible = !this.settings.windows.main.isVisible;
  //   this.toggleCollapse();
  // }

  // toggleCollapse() {
  //   this.settings.windows.main.isVisible = !this.settings.windows.main.isVisible;
  //   if (!this.settings.windows.main.isVisible) this.settings.windows.main.currentHeight = 34;
  //   else this.settings.windows.main.currentHeight = this.settings.windows.main.height;
  // }

  // getItemHeight(list, item) {
  //   var index = list.indexOf(item);
  //   return (index + 1) * 40;
  // }



  // = = = Old code

  userId = 1;


  chatOpenHandler(data) {
  }

  groupCreated(value) {
  }

  groupUpdated(value) {
  }

  sentMessageHandler(value) {
    console.log('msg sent!', value);
  }
}

