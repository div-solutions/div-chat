import { Component, OnInit, Input } from '@angular/core';
import { Status } from '../../models/status-enum';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-participants-tabs',
  templateUrl: './participants-tabs.component.html',
  styleUrls: ['./participants-tabs.component.scss']
})
export class ParticipantsTabsComponent implements OnInit {

  settings;
  @Input() fullscreen = false;

  selectedTab;
  tabs = [];

  constructor(
    private mainService: MainService
  ) { }

  selectTab(id) {
    this.settings = this.mainService.settings;
    this.tabs = this.mainService.tabs;
    this.selectedTab = this.tabs.filter(x => x.id === id)[0];
  }



  ngOnInit(): void {
    this.selectTab('friends');
    if (this.fullscreen === true) {

    }
  }

}
