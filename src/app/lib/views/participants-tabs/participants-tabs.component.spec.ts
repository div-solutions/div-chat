import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantsTabsComponent } from './participants-tabs.component';

describe('ParticipantsTabsComponent', () => {
  let component: ParticipantsTabsComponent;
  let fixture: ComponentFixture<ParticipantsTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantsTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantsTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
