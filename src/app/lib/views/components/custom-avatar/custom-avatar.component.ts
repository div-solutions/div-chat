import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-avatar',
  templateUrl: './custom-avatar.component.html',
  styleUrls: ['./custom-avatar.component.scss']
})
export class CustomAvatarComponent implements OnInit {
  showDefault = true;

  @Input() avatar;
  @Input() size = '18px';

  constructor() { }

  ngOnInit(): void {
    if(this.avatar !== undefined && this.avatar !== "") this.showDefault = false;
  }

}
