import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomStatusBadgeComponent } from './custom-status-badge.component';

describe('CustomStatusBadgeComponent', () => {
  let component: CustomStatusBadgeComponent;
  let fixture: ComponentFixture<CustomStatusBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomStatusBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomStatusBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
