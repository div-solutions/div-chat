import { Component, OnInit, Input } from '@angular/core';
import { Status } from 'src/app/lib/models/status-enum';


@Component({
  selector: 'app-custom-status-badge',
  templateUrl: './custom-status-badge.component.html',
  styleUrls: ['./custom-status-badge.component.scss']
})
export class CustomStatusBadgeComponent implements OnInit {
  @Input() status: Status;
  @Input() size = '18px';
  color = 'grey';

  constructor() { }

  ngOnInit(): void {
    switch (this.status) {
      case Status.Offline:
        this.color = 'grey';
        break;
        case Status.Online:
        this.color = 'green';
        break;
        case Status.Away:
        this.color = 'orange';
        break;
    }
  }

}
