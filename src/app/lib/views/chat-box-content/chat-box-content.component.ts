import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';

import { MessageModel } from './../../models/message-model'

@Component({
  selector: 'app-chat-box-content',
  templateUrl: './chat-box-content.component.html',
  styleUrls: ['./chat-box-content.component.scss']
})
export class ChatBoxContentComponent implements OnInit {
  userId;
  settings;
  messages: MessageModel[] = [];

  newMessageContent = "";

  constructor(
    private mainService: MainService
  ) { }

  ngOnInit(): void {
    this.settings = this.mainService.settings;
    this.userId = this.mainService.userId;

    this.messages.push(new MessageModel('1', 'Vasco', '', 'test'));
    this.messages.push(new MessageModel('2', 'Shantka', '', 'test 1'));

  }

  sendMessage() {
    this.messages.push(new MessageModel(this.userId, '', '', this.newMessageContent));
    this.newMessageContent = "";
  }

}
