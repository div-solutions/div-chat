import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fullscreen',
  templateUrl: './fullscreen.component.html',
  styleUrls: ['./fullscreen.component.scss']
})
export class FullscreenComponent implements OnInit {

  @Input() isVisible = false;

  constructor() { }

  ngOnInit(): void {
  }

}
