import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-embedded',
  templateUrl: './embedded.component.html',
  styleUrls: ['./embedded.component.scss']
})
export class EmbeddedComponent implements OnInit {

  @Input() isVisible;

  constructor() { }

  ngOnInit(): void {
  }

}
