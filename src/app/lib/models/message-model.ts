export class MessageModel {
    constructor(authorId, name, avatar, content) {
        this.authorId = authorId;
        this.authorName = name;
        this.authorAvatar = avatar;
        this.content = content;
    }

    authorId?: string;
    authorName?: string;
    authorAvatar?: string;

    content?: string;
}