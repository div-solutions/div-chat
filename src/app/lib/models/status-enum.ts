export enum Status {
  Online = 1,
  Offline = 2,
  Away = 3
}
