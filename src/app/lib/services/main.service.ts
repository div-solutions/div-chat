import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  userId;

  settings;
  fullscreenMode = false;

  selectedChat;

  tabs;

  constructor() { }
}
